package com.fornova.demo.controllers;

import com.fornova.demo.models.JobStatus;
import com.fornova.demo.models.JobBatch;
import com.fornova.demo.models.JobBatchRecord;
import com.fornova.demo.repo.JobBatchesRepo;
import com.fornova.demo.services.LBThrottler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Optional;

@RestController
public class JobsController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final LBThrottler throttler;
    private final JobBatchesRepo batchesRepo;
    private final RabbitTemplate rabbitTemplate;

    @Value("${exchanges.jobs}")
    private String jobsExchange;

    @Value("${exchanges.job-cancels}")
    private String cancelsExchange;

    @Autowired
    public JobsController(LBThrottler throttler, JobBatchesRepo batchesRepo, RabbitTemplate rabbitTemplate) {
        this.throttler = throttler;
        this.batchesRepo = batchesRepo;
        this.rabbitTemplate = rabbitTemplate;
    }

    @PostConstruct
    public void init() {
        // Implies manual consumer acknowledgements
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (correlationData != null) {
                String msgId = correlationData.getId();
                log.info("Job " + msgId + " confirmed, ack: " + ack);

                batchesRepo.findById(msgId)
                           .filter(job -> job.getStatus() == JobStatus.SUBMITED)
                           .map(job ->
                                batchesRepo.save(job.withStatus(ack ? JobStatus.EXECUTED : JobStatus.FAILED))
                           );
            } else {
                log.warn("Received unknown correlation, cause " + cause);
            }
        });
    }

    @PostMapping("/jobs")
    public ResponseEntity create(@RequestBody @Validated JobBatch job) {

        if (throttler.throttleByProvider(job.getProviderId())) {
            log.warn("Failed to register a job " + job + ". Too many request for provider");
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }

        log.info("Adding new job batch " + job);

        batchesRepo.insert(JobBatchRecord.ofJob(job));

        rabbitTemplate.convertAndSend(jobsExchange, String.valueOf(job.getProviderId()), job, new CorrelationData(job.getId()));

        return ResponseEntity.noContent().build();
    }


    @GetMapping("/providers/{providerId}/batches")
    public Page<JobBatchRecord> getBatchesList(@PathVariable long providerId, Pageable pageable) {
        log.info("Returning batches list for provider " + providerId);
        return batchesRepo.findByJobProviderId(providerId, pageable);
    }

    @GetMapping("/providers/{providerId}/batches/{batchId}")
    public Optional<JobBatchRecord> getBatch(@PathVariable long providerId, @PathVariable long batchId) {
        log.info("Returning batches list for specific batch " + batchId + " provider " + providerId);
        return batchesRepo.findOneByJobProviderIdAndJobBatchId(providerId, batchId);
    }

    /**
     * Cancels all jobs that haven't been processed yet by sending cancel message to a cancel exchange.
     */
    @DeleteMapping("/providers/{providerId}/batches")
    public ResponseEntity cancelBatches(@PathVariable long providerId) {
        log.info("Canceling all batches for provider " + providerId);

        batchesRepo.findByJobProviderIdAndStatus(providerId, JobStatus.SUBMITED).forEach(record -> {
            rabbitTemplate.convertAndSend(cancelsExchange, String.valueOf(record.getJob().getProviderId()), record.getJob());
            batchesRepo.save(record.withStatus(JobStatus.CANCELED));
        });

        return ResponseEntity.noContent().build();
    }

    /**
     * Cancels specific job if it has't been processed yet by sending cancel message to a cancel exchange.
     * For a processed or already canceled message EXPECTATION_FAILED status is returned.
     */
    @DeleteMapping("/providers/{providerId}/batches/{batchId}")
    public ResponseEntity cancelBatches(@PathVariable long providerId, @PathVariable long batchId) {
        log.info("Canceling batch " + batchId + " for provider " + providerId);

        Optional<JobBatchRecord> recordOpt = batchesRepo.findOneByJobProviderIdAndJobBatchId(providerId, batchId)
            .map(record -> {
                if (record.getStatus() == JobStatus.SUBMITED) {
                    rabbitTemplate.convertAndSend(cancelsExchange, String.valueOf(record.getJob().getProviderId()), record.getJob());
                    batchesRepo.save(record.withStatus(JobStatus.CANCELED));
                }
                return record;
        });

        boolean cancelable = recordOpt.map(j -> j.getStatus() == JobStatus.SUBMITED).orElse(false);

        HttpStatus status = recordOpt.isPresent() ?
                (cancelable ? HttpStatus.NO_CONTENT : HttpStatus.EXPECTATION_FAILED) :
                HttpStatus.NOT_FOUND;

        return ResponseEntity.status(status).build();
    }

    /** Handles duplicate job submission. */
    @ResponseStatus(value= HttpStatus.CONFLICT, reason="Job has been already submitted")
    @ExceptionHandler(DuplicateKeyException.class)
    public void duplicateKeyHandler() {}
}
