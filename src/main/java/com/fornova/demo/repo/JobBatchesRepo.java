package com.fornova.demo.repo;


import com.fornova.demo.models.JobBatchRecord;
import com.fornova.demo.models.JobStatus;
import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface JobBatchesRepo extends MongoRepository<JobBatchRecord, String> {

    Page<JobBatchRecord> findByJobProviderId(long providerId, Pageable pageable);
    List<JobBatchRecord> findByJobProviderIdAndStatus(long providerId, JobStatus status);
    Optional<JobBatchRecord> findOneByJobProviderIdAndJobBatchId(long providerId, long batchId);
}
