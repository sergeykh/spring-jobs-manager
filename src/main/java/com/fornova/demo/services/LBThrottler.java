package com.fornova.demo.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Counter based leaky bucket algorithm implementation.
 */
@Service
public class LBThrottler {

    @Value("${providers.throttling.rps.default}")
    private int defaultThrottling;

    private final ConcurrentHashMap<Long, AtomicInteger> buckets = new ConcurrentHashMap<>();

    /**
     * Returns true if request has to be rejected for a specific provider.
     *
     * @param providerId
     * @return true if request has to be rejected for a specific provider.
     */
    public boolean throttleByProvider(long providerId) {
        int maxRps = fetchMaxProviderRps(providerId);
        AtomicInteger atomicCounter = buckets.putIfAbsent(providerId, new AtomicInteger(0));

        if (atomicCounter == null) {
            return false;
        }

        int counter = atomicCounter.incrementAndGet();

        return counter > maxRps;
    }


    /**
     * Returns maximum allowed requests per second for a specific provider.
     * @param providerId
     * @return maximum allowed requests per second for a specific provider.
     */
    public int fetchMaxProviderRps(long providerId) {
        return defaultThrottling;
    }

    /**
     * Decrease bucket counters each second
     */
    @Scheduled(fixedRate = 1000)
    protected void updateCounters() {
        buckets.forEachValue(1, counter -> counter.set(0));
    }

}
