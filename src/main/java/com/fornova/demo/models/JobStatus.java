package com.fornova.demo.models;

public enum JobStatus {
    SUBMITED, EXECUTED, FAILED, CANCELED
}
