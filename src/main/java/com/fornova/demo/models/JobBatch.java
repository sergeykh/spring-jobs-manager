package com.fornova.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class JobBatch {

    @Positive
    private final long providerId;

    @Positive
    private final long batchId;

    @NotBlank
    @Size(max = 4096)
    private final String job;

    public JobBatch(long providerId, long batchId, String job) {
        this.providerId = providerId;
        this.batchId = batchId;
        this.job = job;
    }

    public long getProviderId() {
        return providerId;
    }

    public long getBatchId() {
        return batchId;
    }

    public String getJob() {
        return job;
    }

    @JsonIgnore
    public String getId() {
        return providerId + ":" + batchId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("JobBatch{");
        sb.append("providerId=").append(providerId);
        sb.append(", batchId=").append(batchId);
        sb.append(", job='").append(job).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
