package com.fornova.demo.models;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class JobBatchRecord {
    @Id
    private final String id;
    private final JobBatch job;
    private final Date created;
    private final JobStatus status;

    public JobBatchRecord(String id, JobBatch job, Date created, JobStatus status) {
        this.id = id;
        this.job = job;
        this.created = created;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public JobBatch getJob() {
        return job;
    }

    public Date getCreated() {
        return created;
    }

    public JobStatus getStatus() {
        return status;
    }

    public JobBatchRecord withStatus(JobStatus status) {
        return new JobBatchRecord(id, job, created, status);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("JobBatchRecord{");
        sb.append("id='").append(id).append('\'');
        sb.append(", job=").append(job);
        sb.append(", created=").append(created);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }

    public static JobBatchRecord ofJob(JobBatch job) {
        return new JobBatchRecord(job.getId(), job, new Date(), JobStatus.SUBMITED);
    }
}
