

This app is a jobs management service with REST that gives following functionality:

1. Receive a batches of jobs, each batch contains:

   * Batch ID
   * Provider ID
   * Abstract job (a String)

2. Execute jobs by putting it into an external queue, for example RabbitMQ, executing of a job should be throtelled: for each provider is predefined throttling, for example 2 jobs/sec
3. Give current status of all or specific batches
4. Cancel all or specific batches

Some key decisions:

 * Throttling done in node independent way, so to achieve a fair throttling for multiple nodes it requires a coeficient. But on the other hand it works faster and as it's a security feature it doesn't impact a DB.
 * Exchanges (listed in application.properties) have to be created beforehand
 * Messages are sent in JSON
 * Cancel is sent as a separate message into a different exchange, so client have to listen for cancel messages and stop job execution 